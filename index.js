var express = require("express");
var app = express();
var schedule = require('node-schedule');

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`app listen on port ${PORT}`);
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, apiKey"
  );

  if ("OPTIONS" == req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.get('/', (req, res) => {
  res.send("hello world");
});

var int = 0;

schedule.scheduleJob('5 * * * * *', () => {
  console.log(`schedule notify ${++int}`);
});